#include <iostream>

using namespace std;

void printArray(int array[], int size){


  cout << "[";
  for(int i = 0; i <size; i++){

    cout << array[i];
    cout << " ";
  }
  cout << "]";
}

void swap(int array[],int size, int position1, int position2)
{
  int tempo;

  tempo = array[position2];
  array[position2] = array[position1];
  array[position1]=tempo;   

  printArray(array, size);
}

  
int main()
{
  int size;
  int rarr[]= {1,2,3};

  printArray(rarr, 3);

  swap(rarr, 3, 0, 2);
  
}
