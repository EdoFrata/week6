#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{

  cout << "The name of the program is: " << argv[0] << endl;

  cout << "The arguments are: ";
  
  for(int i = 1; i < argc; i++){

    cout << " " << argv[i];
  }
  
}
